package org.randombits.support.confluence.legacy;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

/**
 * A legacy macro adapter which rewires the old BaseMacro calls to the new
 * com.atlassian.confluence.macro.Macro interface calls, to create an adapter for your
 * macro, you can extend this and implement the getXHtmlMacro() method and getBodyRenderMode() methods
 * as appropriate, the actual logic and functionality will be delegated  to the new macro style.
 *
 * @author Bo Wang
 */
public abstract class LegacyConfluenceMacro extends BaseMacro {

    protected abstract Macro getXHtmlMacro();

    public boolean hasBody() {
        return getXHtmlMacro().getBodyType() != Macro.BodyType.NONE;
    }

    @SuppressWarnings("deprecation")
    public boolean isInline() {
        return getXHtmlMacro().getOutputType() == Macro.OutputType.INLINE;
    }

    @SuppressWarnings("unchecked")
    public String execute( Map params, String body, RenderContext renderContext ) throws MacroException {
        try {
            return getXHtmlMacro().execute( (Map<String, String>) params, body, new DefaultConversionContext( renderContext ) );
        } catch ( MacroExecutionException e ) {
            throw new MacroException( e.getMessage(), e );
        }
    }
}
