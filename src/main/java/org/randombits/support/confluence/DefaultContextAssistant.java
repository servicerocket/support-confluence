/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.support.confluence;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.dashboard.actions.DashboardAction;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.actions.AbstractCreateAndEditPageAction;
import com.atlassian.confluence.pages.actions.ViewPageAction;
import com.atlassian.confluence.pages.templates.PageTemplate;
import com.atlassian.confluence.pages.templates.PageTemplateManager;
import com.atlassian.confluence.plugins.templates.actions.AbstractEditPageTemplateAction;
import com.atlassian.confluence.plugins.templates.actions.AbstractPageTemplateAction;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.user.User;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ActionContext;
import com.opensymphony.xwork.ActionInvocation;
import org.apache.commons.lang.StringUtils;
import org.randombits.support.confluence.ContextAssistant;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Default implementation of {@link ContextAssistant}.
 *
 * @author David Peterson
 */
public class DefaultContextAssistant implements ContextAssistant {

    private static final String DECORATOR_PARAM = "decorator";

    private static final String PRINTABLE_DECORATOR = "printable";

    private final PageTemplateManager pageTemplateManager;

    private final PageManager pageManager;

    public DefaultContextAssistant( PageTemplateManager pageTemplateManager, PageManager pageManager ) {
        this.pageTemplateManager = pageTemplateManager;
        this.pageManager = pageManager;
    }

    /**
     * Returns the page template with the specified name, if it is accessible
     * from the context of the specified spaces, otherwise searches the global
     * space for a matching template and returns that. If no space is specified,
     * only global templates are returned.
     *
     * @param templateName The name of the template.
     * @param space        The space to search, or <code>null</code> to only search
     *                     global templates.
     * @return The page template, or <code>null</code>.
     */
    public PageTemplate getPageTemplate( String templateName, Space space ) {
        PageTemplate pt = null;

        // Try the space
        if ( space != null )
            pt = pageTemplateManager.getPageTemplate( templateName, space );

        // Try global
        if ( pt == null )
            pt = pageTemplateManager.getPageTemplate( templateName, null );

        // Some versions of Confluence don't support the direct method...
        if ( pt == null ) {
            List<PageTemplate> pageTemplates = pageTemplateManager.getGlobalPageTemplates();
            for ( PageTemplate lpt : pageTemplates ) {
                if ( lpt.getName().equals( templateName ) ) {
                    return lpt;
                }
            }

            return null;
        }

        return pt;
    }

    /**
     * Returns the list of all page templates available in the context of the
     * specified spaces, including any global templates. If no space is
     * specified only global templates will be returned.
     *
     * @param space The space to search, or <code>null</code> to only list global
     *              templates.
     * @return The list of available page templates.
     */
    public List<PageTemplate> getPageTemplates( Space space ) {
        List<PageTemplate> templates = pageTemplateManager.getGlobalPageTemplates();
        if ( space != null )
            templates.addAll( pageTemplateManager.getPageTemplates( space ) );
        return templates;
    }

    /**
     * Returns true if the current action is the view option for the current
     * content. If we are not viewing any editable content, false is returned.
     *
     * @param ctx The page context.
     * @return true if the content is in 'view' mode.
     */
    public boolean isViewAction( ConversionContext ctx ) {

        HttpServletRequest req = ServletActionContext.getRequest();
        if ( req == null )
            return false;

        Action action = getCurrentAction();

        ContentEntityObject content = ctx.getEntity();
        if ( content instanceof AbstractPage ) {
            // Page or BlogPost
            if ( action instanceof ViewPageAction )
                return true;
        }
        return false;
    }

    private static Action getCurrentAction() {
        ActionContext ctx = ActionContext.getContext();
        if ( ctx != null ) {
            ActionInvocation ai = ctx.getActionInvocation();
            if ( ai != null )
                return ai.getAction();
        }
        return null;
    }

    /**
     * Returns true if we are editing content.
     *
     * @param ctx The page context.
     * @return true if we are in edit mode.
     */
    public boolean isEditAction( ConversionContext ctx ) {
        Action action = getCurrentAction();
        ContentEntityObject content = ctx.getEntity();
        if ( content instanceof AbstractPage ) {
            if ( action instanceof AbstractCreateAndEditPageAction )
                return true;
        } else if ( content == null ) {
            return isCurrentAction( AbstractEditPageTemplateAction.class );
        }
        return false;
    }

    /**
     * Check if we are on the dashboard.
     *
     * @param ctx The page context.
     * @return <code>true</code> if we are on the dashboard.
     */
    public boolean isDashboardAction( ConversionContext ctx ) {
        return isCurrentAction( DashboardAction.class );
    }

    /**
     * Returns <code>true</code> if the current page context is within a page
     * template action.
     *
     * @param ctx The page context.
     * @return <code>true</code> if we are operating on a template.
     */
    public boolean isTemplateAction( ConversionContext ctx ) {
        return isCurrentAction( AbstractPageTemplateAction.class );
    }

    /**
     * Checks if the specified class is the current action.
     *
     * @param actionClass The class to check.
     * @return <code>true</code> if current action is an instance of the
     *         specified class.
     */
    public boolean isCurrentAction( Class<? extends Action> actionClass ) {
        Action action = getCurrentAction();
        return action != null && actionClass.isInstance( action );
    }

    /**
     * Returns true if the current view is the default display (ie not
     * printable, etc).
     *
     * @param ctx The current page context.
     * @return true if there is no special display in place.
     */
    public boolean isDefaultDisplay( ConversionContext ctx ) {

        HttpServletRequest req = ServletActionContext.getRequest();

        return req != null && StringUtils.isBlank( req.getParameter( DECORATOR_PARAM ) )
                && RenderContextOutputType.DISPLAY.equals( ctx.getOutputType() );

    }

    /**
     * Returns true if the current view is printable - either the standard
     * 'print' view, Word or PDF.
     *
     * @param ctx The page context.
     * @return true if the printable view is being displayed.
     */
    public boolean isPrintableDisplay( ConversionContext ctx ) {
        if ( RenderContextOutputType.PDF.equals( ctx.getOutputType() )
                || RenderContextOutputType.WORD.equals( ctx.getOutputType() ) )
            return true;

        HttpServletRequest req = ServletActionContext.getRequest();

        return req != null && PRINTABLE_DECORATOR.equals( req.getParameter( DECORATOR_PARAM ) );

    }

    public boolean isPDFDisplay( ConversionContext ctx ) {
        return RenderContextOutputType.PDF.equals( ctx.getOutputType() );
    }

    public boolean isWordDisplay( ConversionContext ctx ) {
        return RenderContextOutputType.WORD.equals( ctx.getOutputType() );
    }

    public boolean isPreviewDisplay( ConversionContext ctx ) {
        return RenderContextOutputType.PREVIEW.equals( ctx.getOutputType() );
    }

    public boolean isRecentlyUpdatedFor( ContentEntityObject ceo, Object user ) {
        User realUser = (User) user;
        return ceo instanceof Page && pageManager.isPageRecentlyUpdatedForUser( (Page) ceo, realUser );
    }

    public PageTemplate getCurrentPageTemplate( ConversionContext ctx ) {
        if ( isTemplateAction( ctx ) ) {
            AbstractPageTemplateAction action = (AbstractPageTemplateAction) getCurrentAction();
            return action.getPageTemplate();
        }
        return null;
    }
}
