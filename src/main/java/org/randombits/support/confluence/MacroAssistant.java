package org.randombits.support.confluence;

import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.support.core.param.ParameterAssistant;

/**
 * Provides access to utilities and other required assistants for {@link ConfluenceMacro}s.
 * This is provided so that macros can simply inject this assistant and we can add
 * extra required assistants to it as we go along, without breaking the API for every
 * concrete macro that extends {@link ConfluenceMacro}.
 */
public interface MacroAssistant {

    EnvironmentAssistant getEnvironmentAssistant();

    ParameterAssistant getParameterAssistant();

    XhtmlContent getXhtmlContent();
}
