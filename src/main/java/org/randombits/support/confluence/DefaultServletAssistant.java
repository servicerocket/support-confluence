package org.randombits.support.confluence;

import com.atlassian.user.User;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;
import org.randombits.support.confluence.ServletAssistant;
import org.randombits.support.core.env.EnvironmentAssistant;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Default implementation of the {@link ServletAssistant}.
 */
public final class DefaultServletAssistant implements ServletAssistant {

    private final EnvironmentAssistant environmentAssistant;

    public DefaultServletAssistant( EnvironmentAssistant environmentAssistant ) {
        this.environmentAssistant = environmentAssistant;
    }

    public User getCurrentUser() {
        return environmentAssistant.getValue( User.class );
    }

    public HttpServletRequest getRequest() {
        return environmentAssistant.getValue( HttpServletRequest.class );
    }

    /**
     * Returns the request as a {@link com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper}, if the current
     * request supports it. Otherwise, <code>null</code> is returned.
     *
     * @return The request as a multipart handler, or <code>null</code>.
     */
    public MultiPartRequestWrapper getMultiPartRequest() {
        HttpServletRequest req = getRequest();
        if ( req instanceof MultiPartRequestWrapper )
            return (MultiPartRequestWrapper) req;
        return null;
    }

    /**
     * Returns the current HttpServletResponse, if it's available.
     *
     * @return The response, or <code>null</code>.
     */
    public HttpServletResponse getResponse() {
        return environmentAssistant.getValue( HttpServletResponse.class );
    }

    public ServletConfig getServletConfig() {
        return environmentAssistant.getValue( ServletConfig.class );
    }

    public ServletContext getServletContext() {
        return environmentAssistant.getValue( ServletContext.class );
    }

    public HttpSession getSession() {
        return environmentAssistant.getValue( HttpSession.class );
    }

    /**
     * Returns the full path, including the web application context path, if the
     * HttpServlet request is available.
     *
     * @param webappRelativePath The path from the root of the web application.
     * @return The full path.
     */
    public String getFullPath( String webappRelativePath ) {
        if ( webappRelativePath == null )
            return null;

        HttpServletRequest req = getRequest();
        if ( req != null )
            return req.getContextPath() + webappRelativePath;

        return webappRelativePath;
    }
}
