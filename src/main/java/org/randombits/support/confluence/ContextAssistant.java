/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.support.confluence;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.templates.PageTemplate;
import com.atlassian.confluence.spaces.Space;
import com.opensymphony.xwork.Action;

import java.util.List;

/**
 * @author David Peterson
 */
public interface ContextAssistant {
    /**
     * Returns the page template with the specified name, if it is accessable
     * from the context of the specified spaces, otherwise searches the global
     * space for a matching template and returns that. If no space is specified,
     * only global templates are returned.
     * 
     * @param templateName
     *            The name of the template.
     * @param space
     *            The space to search, or <code>null</code> to only search
     *            global templates.
     * @return The page template, or <code>null</code>.
     */
    PageTemplate getPageTemplate( String templateName, Space space );
    
    /**
     * Returns the list of all page templates available in the context of the
     * specified spaces, including any global templates. If no space is
     * specified only global templates will be returned.
     * 
     * @param space
     *            The space to search, or <code>null</code> to only list global
     *            templates.
     * @return The list of available page templates.
     */
    List<PageTemplate> getPageTemplates( Space space );

    /**
     * Returns true if the current action is the view option for the current
     * content. If we are not viewing any editable content, false is returned.
     * 
     * @param ctx
     *            The page context.
     * @return true if the content is in 'view' mode.
     */
    boolean isViewAction( ConversionContext ctx );
    
    /**
     * Returns true if we are editing content.
     * 
     * @param ctx
     *            The page context.
     * @return true if we are in edit mode.
     */
    boolean isEditAction( ConversionContext ctx );

    /**
     * Check if we are on the dashboard.
     * 
     * @param ctx
     *            The page context.
     * @return <code>true</code> if we are on the dashboard.
     */
    boolean isDashboardAction( ConversionContext ctx );

    /**
     * Returns <code>true</code> if the current page context is within a page
     * template action.
     * 
     * @param ctx
     *            The page context.
     * @return <code>true</code> if we are operating on a template.
     */
    boolean isTemplateAction( ConversionContext ctx );

    /**
     * Checks if the specified class is the current action.
     * 
     * @param actionClass
     *            The class to check.
     * @return <code>true</code> if current action is an instance of the
     *         specified class.
     */
    boolean isCurrentAction( Class<? extends Action> actionClass );

    /**
     * Returns true if the current view is the default display (ie not
     * printable, etc).
     * 
     * @param ctx
     *            The current page context.
     * @return true if there is no special display in place.
     */
    boolean isDefaultDisplay( ConversionContext ctx );
    
    /**
     * Returns true if the current view is printable - either the standard
     * 'print' view, Word or PDF.
     * 
     * @param ctx
     *            The page context.
     * @return true if the printable view is being displayed.
     */
    boolean isPrintableDisplay( ConversionContext ctx );
    
    boolean isPDFDisplay( ConversionContext ctx );

    boolean isWordDisplay( ConversionContext ctx );

    boolean isPreviewDisplay( ConversionContext ctx );

    boolean isRecentlyUpdatedFor( ContentEntityObject ceo, Object user );

    PageTemplate getCurrentPageTemplate( ConversionContext ctx );
}
