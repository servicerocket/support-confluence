/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.support.confluence.sorting;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.links.linktypes.AbstractContentEntityLink;
import com.atlassian.confluence.links.linktypes.AttachmentLink;
import com.atlassian.confluence.pages.Attachment;

/**
 * Compares the creation date of basic Confluence content objects. This
 * comparitor will handle any object, but basically only recognises
 * {@link com.atlassian.confluence.core.ContentEntityObject}s, {@link Attachments} and link objects for those
 * types. You can specify a comparitor is more specify by specifying the
 * <code>T</code> to be a specific type.
 */
public class CreationDateComparator<T> extends BaseDateComparator<T> {
    public CreationDateComparator() {
    }

    /**
     * Returns the creation date of {@link com.atlassian.confluence.core.ContentEntityObject} or
     * {@link com.atlassian.confluence.pages.Attachment} objects as a long. It also supports
     * {@link com.atlassian.confluence.links.linktypes.AbstractContentEntityLink} and {@link com.atlassian.confluence.links.linktypes.AttachmentLink} to those
     * types of objects.
     */
    @Override protected long getTime( T source ) {
        Object object = source;
        if ( source instanceof AttachmentLink )
            object = ( ( AttachmentLink ) source ).getAttachment();
        else if ( source instanceof AbstractContentEntityLink )
            object = ( ( AbstractContentEntityLink ) source ).getDestinationContent();

        if ( object instanceof ContentEntityObject )
            return ( ( ContentEntityObject ) object ).getCreationDate().getTime();
        else if ( source instanceof Attachment )
            return ( ( Attachment ) object ).getCreationDate().getTime();

        return -1;
    }
}
