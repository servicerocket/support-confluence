/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.support.confluence.sorting;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.links.linktypes.AbstractContentEntityLink;
import com.atlassian.confluence.links.linktypes.AttachmentLink;
import com.atlassian.renderer.links.Link;

import java.util.Comparator;

/**
 * This is the base comparator for comparing objects which are connected to
 * {@link com.atlassian.confluence.core.ConfluenceEntityObject}s. Those entities are then the basis on which
 * the two objects are compared.
 * 
 * @author David Peterson
 */
public abstract class BaseComparator<T> implements Comparator<T> {
    public BaseComparator() {
    }

    protected ConfluenceEntityObject getEntity( T source ) {
        if ( source instanceof Link ) {
            if ( source instanceof AbstractContentEntityLink ) {
                return ( ( AbstractContentEntityLink ) source ).getDestinationContent();
            } else if ( source instanceof AttachmentLink ) {
                return ( ( AttachmentLink ) source ).getAttachment();
            }
        } else if ( source instanceof ConfluenceEntityObject ) {
            return ( ConfluenceEntityObject ) source;
        }

        return null;
    }
}
