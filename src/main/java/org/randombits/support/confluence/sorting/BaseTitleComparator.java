/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.support.confluence.sorting;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.links.linktypes.AbstractContentEntityLink;
import com.atlassian.confluence.links.linktypes.AbstractPageLink;
import com.atlassian.confluence.links.linktypes.AttachmentLink;
import com.atlassian.confluence.links.linktypes.ShortcutLink;
import com.atlassian.renderer.links.Link;
import com.atlassian.renderer.links.UrlLink;

/**
 * TODO: Document this class.
 * 
 * @author David Peterson
 */
public abstract class BaseTitleComparator<T> extends BaseComparator<T> {
    public int compare( T o1, T o2 ) {
        return compare( getTitle( o1 ), getTitle( o2 ) );
    }

    protected abstract int compare( String title1, String title2 );

    protected String getTitle( T object ) {
        String title = null;

        if ( object instanceof Link ) {
            if ( object instanceof AbstractContentEntityLink ) {
                if ( object instanceof AbstractPageLink ) {
                    title = ( ( AbstractPageLink ) object ).getPageTitle();
                } else {
                    title = ( ( AbstractContentEntityLink ) object ).getDestinationContent().getDisplayTitle();
                }
            } else if ( object instanceof AttachmentLink ) {
                title = ( ( AttachmentLink ) object ).getAttachment().getDisplayTitle();
            } else if ( object instanceof ShortcutLink || object instanceof UrlLink ) {
                title = ( ( Link ) object ).getLinkBody();
            } else {
                title = ( ( Link ) object ).getTitle();
            }
        } else if ( object instanceof ContentEntityObject ) {
            title = ( ( ContentEntityObject ) object ).getTitle();
        }

        return ( title == null ) ? "" : title;
    }
}
