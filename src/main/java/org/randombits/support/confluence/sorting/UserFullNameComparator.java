package org.randombits.support.confluence.sorting;

import com.atlassian.user.User;

import java.util.Comparator;

/**
 * Compares the full name of two users. Any actual instance is considered to be
 * before a <code>null</code>.
 * 
 * @author David Peterson
 */
public class UserFullNameComparator implements Comparator<User> {

    public int compare( User u1, User u2 ) {
        if ( u1 == null )
            return u2 == null ? 0 : -1;
        if ( u2 == null )
            return 1;
        return u1.getFullName().compareTo( u2.getFullName() );
    }

}
