package org.randombits.support.confluence.render;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.BodyContent;
import com.atlassian.confluence.core.BodyType;
import com.atlassian.confluence.renderer.PageContext;

/**
 * Represents a {@link com.atlassian.confluence.core.BodyContent} as a {@link Renderable} instance.
 */
public class BodyContentRenderable implements Renderable {

    private final BodyContent bodyContent;

    private final RenderType renderType;

    public BodyContentRenderable( BodyContent bodyContent ) {
        this.bodyContent = bodyContent;

        BodyType bodyType = bodyContent.getBodyType();
        if ( BodyType.XHTML.equals( bodyType ) ) {
            renderType = RenderType.RICHTEXT;
        } else if ( BodyType.WIKI.equals( bodyType ) ) {
            renderType = RenderType.WIKI;
        } else {
            renderType = RenderType.PLAIN;
        }
    }


    @Override
    public String getText() {
        return bodyContent.getBody();
    }

    @Override
    public RenderType getType() {
        return renderType;
    }

    @Override
    public ConversionContext getConversionContext() {
        PageContext ctx = null;
        if ( bodyContent != null && bodyContent.getContent() != null )
            ctx = bodyContent.getContent().toPageContext();

        if ( ctx == null )
            ctx = new PageContext();

        return new DefaultConversionContext( ctx );
    }
}
