package org.randombits.support.confluence.render;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Thrown when there is a problem while rendering a  {@link Renderable} value.
 */
public class RenderException extends Exception {

    List<Throwable> exceptions = new ArrayList<Throwable>();

    public RenderException() {
    }

    public RenderException( String message ) {
        super( message );
    }

    public RenderException( String message, Throwable cause ) {
        super( message, cause );
        exceptions.add( cause );
    }

    public RenderException( Throwable cause ) {
        super( cause );
        exceptions.add( cause );
    }

    public RenderException( String message, Collection<? extends Throwable> exceptions ) {
        super( message );
        this.exceptions.addAll( exceptions );
    }

    public Collection<Throwable> getExceptions() {
        return exceptions;
    }
}
