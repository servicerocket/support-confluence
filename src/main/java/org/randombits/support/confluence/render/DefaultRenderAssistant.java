package org.randombits.support.confluence.render;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.macro.Macro.OutputType;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.components.HtmlEscaper;
import org.randombits.support.confluence.render.*;
import org.randombits.support.confluence.util.XhtmlUtils;

import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of  {@link org.randombits.support.confluence.render.RenderAssistant}.
 */
public class DefaultRenderAssistant implements RenderAssistant {

    private final XhtmlContent xhtmlContent;

    public DefaultRenderAssistant( XhtmlContent xhtmlContent ) {
        this.xhtmlContent = xhtmlContent;
    }

    @Override
    public String render( String text, RenderType type, ConversionContext conversionContext ) throws RenderException {
        return render( new DefaultRenderable( text, type, conversionContext ) );
    }

    @Override
    public String render( Renderable renderable ) throws RenderException {
        switch ( renderable.getType() ) {
            case WIKI:
                return renderWiki( renderable );
            case RICHTEXT:
                return renderRichText( renderable );
            default:
                return renderPlain( renderable );
        }
    }

    private String renderPlain( Renderable renderable ) {
        return HtmlEscaper.escapeAll( renderable.getText(), false );
    }

    private String renderWiki( Renderable renderable ) throws RenderException {
        List<RuntimeException> exceptions = new ArrayList<RuntimeException>();
        try {
            String value = xhtmlContent.convertWikiToView( renderable.getText(), renderable.getConversionContext(), exceptions );
            if ( !exceptions.isEmpty() ) {
                throw new RenderException( "Exceptions occurred while rendering wiki markup.", exceptions );
            }
            
            return  XhtmlUtils.stripParagraphWrapper(value); 
        } catch ( XMLStreamException e ) {
            throw new RenderException( e.getMessage(), e );
        } catch ( XhtmlException e ) {
            throw new RenderException( e.getMessage(), e );
        }
    }

    private String renderRichText( Renderable renderable ) throws RenderException {
        try {
            return XhtmlUtils.stripParagraphWrapper( 
            		xhtmlContent.convertStorageToView( renderable.getText(), renderable.getConversionContext()) );
        } catch ( XMLStreamException e ) {
            throw new RenderException( e.getMessage(), e );
        } catch ( XhtmlException e ) {
            throw new RenderException( e.getMessage(), e );
        }
    }
}
