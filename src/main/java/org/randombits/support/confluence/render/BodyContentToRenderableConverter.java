package org.randombits.support.confluence.render;

import com.atlassian.confluence.core.BodyContent;
import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

/**
 * Converts a {@link com.atlassian.confluence.core.BodyContent} value to a {@link Renderable} instance.
 */
public class BodyContentToRenderableConverter extends AbstractConverter<BodyContent, Renderable> {

    public BodyContentToRenderableConverter() {
        super( ConversionCost.CREATE_SIMPLE_OBJECT );
    }

    @Override
    protected Renderable convert( BodyContent bodyContent ) throws ConversionException {
        return new BodyContentRenderable( bodyContent );
    }
}
