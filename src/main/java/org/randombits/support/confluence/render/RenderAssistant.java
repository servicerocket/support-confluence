package org.randombits.support.confluence.render;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;

/**
 * Provides a simple interface for rendering a value of a particular type.
 */
public interface RenderAssistant {

    String render( String text, RenderType type, ConversionContext conversionContext ) throws RenderException;

    String render( Renderable renderable ) throws RenderException;
}
