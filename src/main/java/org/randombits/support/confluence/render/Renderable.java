package org.randombits.support.confluence.render;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;

/**
 * Represents a value that is renderable. Other plugins that wish to provided {@link Renderable} versions
 * of objects should register {@link org.randombits.support.core.convert.Converter} classes to convert
 * the custom object into a Renderable object.
 */
public interface Renderable {

    String getText();

    RenderType getType();

    ConversionContext getConversionContext();
}
