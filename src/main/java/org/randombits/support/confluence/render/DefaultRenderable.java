package org.randombits.support.confluence.render;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.renderer.PageContext;

/**
 * Allows manual construction of a {@link Renderable} value, where all the relevant pieces
 * are provided at construction.
 */
public class DefaultRenderable implements Renderable {

    private final String text;

    private final RenderType renderType;

    private final ConversionContext conversionContext;

    /**
     * Constructs an instance with the specified text, set to {@link RenderType#PLAIN} and
     * a default ConversionContext.
     *
     * @param text The text.
     */
    public DefaultRenderable( String text ) {
        this( text, RenderType.PLAIN, new DefaultConversionContext( new PageContext() ) );
    }

    /**
     * Constructs an instance with the specified text, renderType and conversion context.
     *
     * @param text              The text value.
     * @param renderType              The renderType of render to perform.
     * @param conversionContext The conversion context to perform it in.
     */
    public DefaultRenderable( String text, RenderType renderType, ConversionContext conversionContext ) {
        this.conversionContext = conversionContext;
        this.renderType = renderType;
        this.text = text;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public RenderType getType() {
        return renderType;
    }

    @Override
    public ConversionContext getConversionContext() {
        return conversionContext;
    }
}
