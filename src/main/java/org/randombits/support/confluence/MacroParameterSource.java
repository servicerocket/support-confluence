package org.randombits.support.confluence;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import org.randombits.support.core.param.ParameterContext;
import org.randombits.support.core.param.source.MapParameterSource;

import java.util.Map;

/**
 * Provides parameter details for a {@link MacroInfo} instance. {@link ParameterContext}s
 * created by this source will be pre-populated with the current {@link ConversionContext}
 * and {@link ContentEntityObject} from the {@link MacroInfo} instance.
 */
public class MacroParameterSource extends MapParameterSource {

    private final ConversionContext conversionContext;

    private final ContentEntityObject content;

    public MacroParameterSource( MacroInfo info ) {
        this( info.getMacroParamsMap(), info.getConversionContext(), info.getContent() );
    }

    public MacroParameterSource( Map<String, String> params, ConversionContext context ) {
        this( params, context, context.getEntity() );
    }

    public MacroParameterSource( Map<String, String> params, ConversionContext context, ContentEntityObject content ) {
        super( params );
        this.conversionContext = context;
        this.content = content;
    }

    @Override
    public ParameterContext createContext() {
        ParameterContext context = super.createContext();
        context.set( ConversionContext.class, conversionContext );
        context.set( ContentEntityObject.class, content );
        return context;
    }
}
