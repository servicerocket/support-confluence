package org.randombits.support.confluence.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.confluence.macro.Macro.OutputType;

import static com.atlassian.confluence.util.GeneralUtil.*;

/**
 * Provides utilities for working with XHTML.
 */
public class XhtmlUtils {

    private static Pattern BLANK_PARA = Pattern.compile( "(\\s|[\\u00A0]|\\<\\/?[pP]/?\\>)*" );

    private static Pattern STRIP_PARA = Pattern.compile( "^(?:\\s|[\\u00A0]|\\<\\/?[pP]/?\\>)*(.*?)(?:\\s|[\\u00A0]|\\<\\/?[pP]/?\\>)*$", Pattern.DOTALL );

    public static boolean isNotBlank( String value ) {
        return !isBlank( value );
    }

    public static String stripParagraphWrapper( String xhtmlValue ) {
        Matcher matcher = STRIP_PARA.matcher( xhtmlValue );
        if ( matcher.matches() ) {
            return matcher.group( 1 );
        } else {
            return xhtmlValue;
        }
    }

    public static boolean isBlank( String value ) {
        return value == null || BLANK_PARA.matcher( value ).replaceAll( "" ).isEmpty();
    }

    public static void appendAttribute( StringBuilder out, String name, Object value ) {
        if ( value != null )
            out.append( " " ).append( escapeXml( name ) ).append( "='" ).append( escapeXml( value.toString() ) )
                    .append( "'" );
    }

    public static void appendStyleParam( StringBuilder out, String name, String value ) {
        if ( value != null )
            out.append( escapeXml( name ) ).append( ": " ).append( escapeXml( value ) ).append( "; " );
    } 
}
