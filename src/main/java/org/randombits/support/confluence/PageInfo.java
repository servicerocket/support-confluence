/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.support.confluence;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;
import org.randombits.storage.Storage;
import org.randombits.storage.confluence.ContentPropertyStorage;
import org.randombits.support.core.env.EnvironmentAssistant;

import javax.servlet.http.HttpServletRequest;

/**
 * Represents a page (or more specifically, a ContentEntityObject) execution context.
 *
 * @author David Peterson
 */
public class PageInfo extends RequestInfo {

    private static final ThreadLocal<ContentEntityObject> contentContext = new ThreadLocal<ContentEntityObject>();

    private ContentEntityObject content;

    private Storage contentProperties;

    public PageInfo( HttpServletRequest req, ContentEntityObject content ) {
        super( req );
        setContent( content );
    }

    public PageInfo( EnvironmentAssistant environmentAssistant, ContentEntityObject content ) {
        super( environmentAssistant );
        setContent( content );
    }

    /**
     * Returns the current content that is being executed in a Macro environment.
     *
     * @return The current content entity object.
     */
    public static ContentEntityObject getCurrentContent() {
        return contentContext.get();
    }

    protected void setContent( ContentEntityObject content ) {
        this.content = content;
        contentContext.set( content );
    }

    /**
     * @return the ContentEntityObject this macro is being executed in.
     */
    public ContentEntityObject getContent() {
        return content;
    }

    /**
     * Returns a storage instance allowing access to the content properties for
     * the current ContentEntityObject. If the content entity object is not set,
     * <code>null</code> is returned.
     *
     * @return The content property storage.
     */
    public Storage getContentProperties() {
        if ( contentProperties == null && content != null ) {
            contentProperties = new ContentPropertyStorage( getContent() );
        }

        return contentProperties;
    }

    /**
     * Returns the current space.
     *
     * @return The current space.
     */
    public Space getSpace() {
        ContentEntityObject content = getContent();
        if ( content instanceof SpaceContentEntityObject ) {
            return ( (SpaceContentEntityObject) content ).getSpace();
        }
        return null;
    }

    public User getCurrentUser() {
        return AuthenticatedUserThreadLocal.getUser();
    }

    public void close() {
        super.close();

        if ( contentContext.get() == content )
            contentContext.remove();

        content = null;
        contentProperties = null;
    }
}
