package org.randombits.support.confluence.style;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.SpaceAware;
import com.atlassian.confluence.themes.ColourScheme;
import com.atlassian.confluence.themes.ColourSchemeManager;
import com.atlassian.confluence.themes.Theme;
import com.atlassian.confluence.themes.ThemeManager;
import org.apache.commons.lang.StringUtils;

/**
 * This is an action which will render a CSS/VM file appropriately for
 * Confluence. It is pre-populated with useful helpers for style and theme
 * information.
 * 
 * <p>
 * To add it to your plugin, put something like this in your
 * <code>atlassian-plugin.xml</code> file:
 * 
 * <pre>
 *  &lt;xwork name=&quot;Extra Plugin Actions&quot; key=&quot;extraPluginActions&quot;&gt;
 *  &lt;package name=&quot;scaffold&quot; extends=&quot;default&quot; namespace=&quot;/plugins/yourplugin&quot;&gt;
 *  &lt;default-interceptor-ref name=&quot;defaultStack&quot;/&gt;
 * 
 *  &lt;action name=&quot;style&quot; class=&quot;net.customware.confluence.support.style.StyleAction&quot;&gt;
 *  &lt;result name=&quot;success&quot; type=&quot;velocity-css&quot;&gt;/path/to/css/style.css.vm&lt;/result&gt;
 *  &lt;/action&gt;    
 *  &lt;/package&gt;
 *  &lt;/xwork&gt;
 * </pre>
 */
public class StyleAction extends ConfluenceActionSupport implements SpaceAware {

    /* Pre 2.8 Custom Colours */
    private static final String CUSTOM_1 = "property.style.customcolour1";

    private static final String CUSTOM_2 = "property.style.customcolour2";

    private static final String CUSTOM_3 = "property.style.customcolour3";

    private static final String CUSTOM_4 = "property.style.customcolour4";

    private static final String CUSTOM_5 = "property.style.customcolour5";

    /* Post 2.8 extra colours */
    private static final String MENU_ITEM_SELECTED_BACKGROUND = "property.style.menuitemselectedbgcolour";

    private static final String MENU_ITEM_SELECTED_TEXT = "property.style.menuitemselectedtextcolour";

    private static final String MENU_ITEM_TEXT = "property.style.menuitemtextcolour";

    private static final String MENU_SELECTED_BACKGROUND = "property.style.menuselectedbgcolour";

    private static final String TOP_BAR_MENU_ITEM_TEXT = "property.style.topbarmenuitemtextcolour";

    private static final String TOP_BAR_MENU_SELECTED_BACKGROUND = "property.style.topbarmenuselectedbgcolour";

    private ColourScheme scheme;

    private ColourSchemeManager colourSchemeManager;

    private ThemeManager themeManager;

    private Theme theme;

    private Space space;

    @Override public boolean isPermitted() {
        // Stylesheets are visible to everyone.
        return true;
    }

    @Override public String execute() {
        scheme = getActiveColourScheme( getSpaceKey() );
        theme = getActiveTheme( getSpaceKey() );
        return SUCCESS;
    }

    private Theme getActiveTheme( String spaceKey ) {
        if ( StringUtils.isNotEmpty( spaceKey ) ) {
            return themeManager.getSpaceTheme( spaceKey );
        }
        return themeManager.getGlobalTheme();
    }

    private ColourScheme getActiveColourScheme( String spaceKey ) {
        if ( StringUtils.isNotEmpty( spaceKey ) ) {
            return colourSchemeManager.getSpaceColourScheme( spaceKey );
        }
        return colourSchemeManager.getGlobalColourScheme();
    }

    public Theme getTheme() {
        return theme;
    }

    public String getTopBarColor() {
        return scheme.get( ColourScheme.TOP_BAR );
    }

    public String getBreadcrumbsTextColour() {
        return scheme.get( ColourScheme.BREADCRUMBS_TEXT );
    }

    public String getSpaceNameColor() {
        return scheme.get( ColourScheme.SPACE_NAME );
    }

    public String getHeadingTextColor() {
        return scheme.get( ColourScheme.HEADING_TEXT );
    }

    public String getLinkColor() {
        return scheme.get( ColourScheme.LINK );
    }

    public String getBorderColor() {
        return scheme.get( ColourScheme.BORDER );
    }

    public String getNavBackgroundColor() {
        return scheme.get( ColourScheme.NAV_BACKGROUND );
    }

    public String getNavTextColor() {
        return scheme.get( ColourScheme.NAV_TEXT );
    }

    public String getNavSelectedBackgroundColor() {
        return scheme.get( ColourScheme.NAV_SELECTED_BACKGROUND );
    }

    public String getNavSelectedTextColor() {
        return scheme.get( ColourScheme.NAV_SELECTED_TEXT );
    }

    public String getCustomColor1() {
        return scheme.get( CUSTOM_1 );
    }

    public String getCustomColor2() {
        return scheme.get( CUSTOM_2 );
    }

    public String getCustomColor3() {
        return scheme.get( CUSTOM_3 );
    }

    public String getCustomColor4() {
        return scheme.get( CUSTOM_4 );
    }

    public String getCustomColor5() {
        return scheme.get( CUSTOM_5 );
    }

    public String getTopBarColour() {
        return scheme.get( ColourScheme.TOP_BAR );
    }

    public String getSpaceNameColour() {
        return scheme.get( ColourScheme.SPACE_NAME );
    }

    public String getHeadingTextColour() {
        return scheme.get( ColourScheme.HEADING_TEXT );
    }

    public String getLinkColour() {
        return scheme.get( ColourScheme.LINK );
    }

    public String getBorderColour() {
        return scheme.get( ColourScheme.BORDER );
    }

    public String getNavBackgroundColour() {
        return scheme.get( ColourScheme.NAV_BACKGROUND );
    }

    public String getNavTextColour() {
        return scheme.get( ColourScheme.NAV_TEXT );
    }

    public String getNavSelectedBackgroundColour() {
        return scheme.get( ColourScheme.NAV_SELECTED_BACKGROUND );
    }

    public String getNavSelectedTextColour() {
        return scheme.get( ColourScheme.NAV_SELECTED_TEXT );
    }

    public String getMenuItemSelectedBackgroundColor() {
        return scheme.get( MENU_ITEM_SELECTED_BACKGROUND );
    }

    public String getMenuItemSelctedTextColor() {
        return scheme.get( MENU_ITEM_SELECTED_TEXT );
    }

    public String getMenuItemTextColor() {
        return scheme.get( MENU_ITEM_TEXT );
    }

    public String getMenuSelectedBackgroundColor() {
        return scheme.get( MENU_SELECTED_BACKGROUND );
    }

    public String getTopBarMenuItemTextColor() {
        return scheme.get( TOP_BAR_MENU_ITEM_TEXT );
    }

    public String getTopBarMenuItemSelectedBackgroundColor() {
        return scheme.get( TOP_BAR_MENU_SELECTED_BACKGROUND );
    }

    public void setColourSchemeManager( ColourSchemeManager colourSchemeManager ) {
        this.colourSchemeManager = colourSchemeManager;
    }

    public void setThemeManager( ThemeManager themeManager ) {
        this.themeManager = themeManager;
    }

    public boolean isSpaceRequired() {
        return false;
    }

    public boolean isViewPermissionRequired() {
        return true;
    }

    public String getSpaceKey() {
        return ( space != null ) ? space.getKey() : null;
    }

    public void setSpace( Space space ) {
        this.space = space;
    }

    public Space getSpace() {
        return space;
    }
}
