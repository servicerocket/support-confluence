package org.randombits.support.confluence;

import org.randombits.storage.Storage;
import org.randombits.storage.servlet.RequestAttributeStorage;
import org.randombits.storage.servlet.RequestParameterStorage;
import org.randombits.storage.servlet.SessionStorage;
import org.randombits.support.core.env.EnvironmentAssistant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RequestInfo {

    private Storage reqParams;

    private RequestAttributeStorage reqAttrs;

    private SessionStorage sessionStorage;

    private HttpServletRequest req;

    private HttpSession session;

    private EnvironmentAssistant environmentAssistant;

    /**
     * Constructs a new RequestInfo with a specific HTTP servlet request
     * instance.
     *
     * @param req The HTTP servlet request.
     */
    public RequestInfo( HttpServletRequest req ) {
        this.req = req;
    }

    /**
     * Constructs a new RequestInfo with no specified HTTP servlet request
     * instance. The instance will be discovered when required via
     * {@link ServletAssistant#getRequest()}.
     *
     * @param environmentAssistant The servlet assistant.
     */
    public RequestInfo( EnvironmentAssistant environmentAssistant ) {
        this.environmentAssistant = environmentAssistant;
    }

    protected EnvironmentAssistant getEnvironmentAssistant() {
        return environmentAssistant;
    }

    /**
     * Retrieves a read-only Storage instance providing access to the request
     * parameters. This may be <code>null</code> if the HTTP request is not
     * available in the current context.
     *
     * @return The request parameter storage instance, or <code>null</code>.
     */
    public Storage getRequestParams() {
        if ( reqParams == null && getRequest() != null ) {
            reqParams = new RequestParameterStorage( getRequest() );
        }
        return reqParams;
    }

    /**
     * Retrieves a Storage instance providing access to the request attributes.
     * This may be <code>null</code> if the HTTP request is not available in the
     * current context.
     *
     * @return The request attribute storage instance, or <code>null</code>.
     */
    public Storage getRequestAttributes() {
        if ( reqAttrs == null ) {
            HttpServletRequest req = getRequest();
            if ( req != null ) {
                reqAttrs = new RequestAttributeStorage( req );
            }
        }

        return reqAttrs;
    }

    /**
     * The HTTP page request object.
     *
     * @return The request object.
     */
    public HttpServletRequest getRequest() {
        if ( req == null )
            req = environmentAssistant.getValue( HttpServletRequest.class );

        return req;
    }

    public HttpSession getSession() {
        if ( session == null ) {
            HttpServletRequest req = getRequest();
            if ( req != null ) {
                session = req.getSession();
            }
        }
        return session;
    }

    /**
     * Returns a storage instance for the current session. This storage is reset
     * for each macro - any boxes opened or closed will not be carried over to
     * the next macro.
     *
     * @return The session storage object.
     */
    public Storage getSessionAttributes() {
        if ( sessionStorage == null ) {
            HttpSession session = getSession();
            if ( session != null ) {
                sessionStorage = new SessionStorage( session );
            }
        }
        return sessionStorage;
    }

    /**
     * Call this method to indicate the class can free up its resources. Any call
     * to methods on this instance after calling close may throw unexpected errors.
     */
    public void close() {
        req = null;
        reqAttrs = null;
        reqParams = null;
        environmentAssistant = null;
        session = null;
        sessionStorage = null;
    }

    @Override
    protected void finalize() throws Throwable {
        close();
    }

}