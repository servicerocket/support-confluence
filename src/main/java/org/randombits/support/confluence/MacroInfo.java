/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.support.confluence;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.randombits.storage.EmptyStorage;
import org.randombits.storage.IndexedStorage;
import org.randombits.storage.MapStorage;
import org.randombits.storage.Storage;
import org.randombits.storage.confluence.ConversionContextStorage;
import org.randombits.storage.confluence.MacroParameterStorage;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.support.core.param.ParameterAssistant;
import org.randombits.support.core.param.ParameterSource;
import org.randombits.support.core.param.Parameters;
import org.randombits.utils.lang.API;

import java.util.*;

/**
 * This class provides some convenience functions commonly used in macros.
 *
 * @author David Peterson
 */
public class MacroInfo extends PageInfo {

    /**
     * This is the parameter value that is used to store the macro body in the macro parameter Map.
     */
    public static final String MACRO_BODY = "@body";

    private static final ThreadLocal<ConversionContext> CONVERSION_CONTEXT_THREAD_LOCAL = new ThreadLocal<ConversionContext>();

    public static ConversionContext getCurrentConversionContext() {
        return CONVERSION_CONTEXT_THREAD_LOCAL.get();
    }

    private final MacroAssistant macroAssistant;

    private final ParameterAssistant parameterAssistant;

    private final String body;

    private Map<String, String> macroParamsMap;

    private IndexedStorage macroParams;

    private XhtmlContent xhtmlContent;

    protected ConversionContext conversionContext;

    private Storage contextParams;

    private MacroExecutionContext macroExecutionContext;

    /**
     * @param conversionContext    The page context the macro is being rendered in.
     * @param environmentAssistant The environment assistant.
     * @param xhtmlContent         The xhtmlContent.
     * @param body                 The body of the macro.
     * @param params               The macro parameter map.
     * @throws com.atlassian.confluence.macro.MacroExecutionException
     *          if there is a problem processing the parameters.
     * @deprecated Use {@link #MacroInfo(java.util.Map, String, com.atlassian.confluence.content.render.xhtml.ConversionContext, MacroAssistant)} instead.
     */
    @SuppressWarnings("deprecation")
    @Deprecated
    public MacroInfo( Map<String, String> params, String body, ConversionContext conversionContext,
                      EnvironmentAssistant environmentAssistant, XhtmlContent xhtmlContent ) throws MacroExecutionException {
        this( createParameterStorage( params ), body, conversionContext, environmentAssistant, xhtmlContent );
    }

    /**
     * Constructs a MacroInfo.
     *
     * @param params
     * @param body
     * @param conversionContext
     * @param environmentAssistant
     * @param xhtmlContent
     * @deprecated Use {@link #MacroInfo(org.randombits.storage.IndexedStorage, String, com.atlassian.confluence.content.render.xhtml.ConversionContext, MacroAssistant)} instead.
     */
    @Deprecated
    public MacroInfo( IndexedStorage params, String body, ConversionContext conversionContext, EnvironmentAssistant environmentAssistant, XhtmlContent xhtmlContent ) {
        this( params, body, conversionContext, new DefaultMacroAssistant( environmentAssistant, xhtmlContent, null ) );
    }

    /**
     * Constructs a MacroInfo.
     *
     * @param macroParams
     * @param body
     * @param info
     * @param environmentAssistant
     * @param xhtmlContent
     * @deprecated Use {@link #MacroInfo(org.randombits.storage.IndexedStorage, String, com.atlassian.confluence.content.render.xhtml.ConversionContext, MacroAssistant)} instead.
     */
    @SuppressWarnings("deprecation")
    @Deprecated
    public MacroInfo( IndexedStorage macroParams, String body, MacroInfo info, EnvironmentAssistant environmentAssistant, XhtmlContent xhtmlContent ) {
        this( macroParams, body, info.getConversionContext(), environmentAssistant, xhtmlContent );
    }

    /**
     * Constructs a new copy of the specified macro info. All storage
     *
     * @param info The macro info to clone.
     */
    @SuppressWarnings("deprecation")
    public MacroInfo( MacroInfo info ) {
        this( info.macroParams, info.body, info.conversionContext, info.getMacroAssistant() );
    }

    public MacroInfo( IndexedStorage params, String body, ConversionContext conversionContext, MacroAssistant macroAssistant ) {
        this( getMapFromStorage( params ), body, conversionContext, macroAssistant );
        this.macroParams = params;
    }

    /**
     * @param conversionContext The page context the macro is being rendered in.
     * @param macroAssistant    The environment assistant.
     * @param body              The body of the macro.
     * @param params            The macro parameter map.
     * @throws com.atlassian.confluence.macro.MacroExecutionException
     *          if there is a problem processing the parameters.
     */
    public MacroInfo( Map<String, String> params, String body, ConversionContext conversionContext,
                      MacroAssistant macroAssistant ) {
        super( macroAssistant.getEnvironmentAssistant(), conversionContext.getEntity() );
        this.macroAssistant = macroAssistant;
        this.macroParamsMap = params;
        this.body = body;
        this.xhtmlContent = macroAssistant.getXhtmlContent();
        this.parameterAssistant = macroAssistant.getParameterAssistant();
        setConversionContext( conversionContext );

        // Add the body to the parameter map.
        macroParamsMap.put( MACRO_BODY, body );
    }

    public MacroInfo( IndexedStorage macroParams, String body, MacroInfo info, MacroAssistant macroAssistant ) {
        this( macroParams, body, info.getConversionContext(), macroAssistant );
    }

    /**
     * Creates a new {@link org.randombits.storage.IndexedStorage}.
     *
     * @param params The parameter map.
     * @return The storage instance.
     * @throws com.atlassian.confluence.macro.MacroExecutionException
     *          if there is a problem.
     */
    public static IndexedStorage createParameterStorage( Map<String, String> params ) {
        return new MacroParameterStorage( params );
    }

    /**
     * Get all the Xhtml MacroDefinitions on this page
     * See 3.2 of - http://confluence.atlassian.com/display/CONFDEV/Creating+a+new+Confluence+4.0+Macro
     *
     * @return a list of MacroDefinitions
     * @throws com.atlassian.confluence.content.render.xhtml.XhtmlException
     *          if t here is an issue parsing the macro definition list.
     * @deprecated No replacement. Inject {@link XhtmlContent} and handle it directly if required.
     */
    @SuppressWarnings("unchecked")
    @Deprecated
    public List<MacroDefinition> getPageMacros() throws XhtmlException {
        if ( xhtmlContent != null ) {
            final List<MacroDefinition> macros = new ArrayList<MacroDefinition>();
            ContentEntityObject pageCEO = conversionContext.getEntity();
            String pageContent = pageCEO.getBodyAsString();

            xhtmlContent.handleMacroDefinitions( pageContent, conversionContext, new MacroDefinitionHandler() {
                public void handle( MacroDefinition macroDefinition ) {
                    macros.add( macroDefinition );
                }
            } );

            return macros;
        }
        return Collections.EMPTY_LIST;
    }

    public MacroExecutionContext asMacroExecutionContext() {
        if ( macroExecutionContext == null ) {
            @SuppressWarnings("unchecked")
            Map<String, String> params = getMapFromStorage( macroParams );
            macroExecutionContext = new MacroExecutionContext( params, getMacroBody(), getConversionContext().getPageContext() );
        }
        return macroExecutionContext;
    }

    private static Map getMapFromStorage( Storage storage ) {
        if ( storage instanceof MapStorage )
            return ( (MapStorage) storage ).getBaseMap();
        if ( storage instanceof MacroParameterStorage )
            return ( (MacroParameterStorage) storage
            ).getOriginalParams();

        return new HashMap<String, String>();
    }

    /**
     * @return the body of the macro.
     */
    @API("4.0.0")
    public String getMacroBody() {
        return body;
    }

    /**
     * @return a storage object allowing access to the macro's parameters.
     */
    @API("4.0.0")
    public IndexedStorage getMacroParams() {
        if ( macroParams == null ) {
            macroParams = createParameterStorage( macroParamsMap );
        }
        return macroParams;
    }

    public <T extends Parameters> T getMacroParams( Class<T> paramsClass ) {
        return parameterAssistant.createParameters( paramsClass, createParameterSource() );
    }

    protected ParameterSource createParameterSource() {
        return new MacroParameterSource( this );
    }

    /**
     * Returns the original macro parameter map. If the MacroInfo was created
     * with a constructor that passed in a {@link IndexedStorage} which was unable to
     * be turned back into the original map, this may return an empty Map even though
     * there are actually parameters via {@link #getMacroParams()}.
     *
     * @return The macro parameter map.
     */
    public Map<String, String> getMacroParamsMap() {
        return macroParamsMap;
    }

    public ConversionContext getConversionContext() {
        return conversionContext;
    }

    public Storage getConversionContextParams() {
        if ( contextParams == null ) {
            contextParams = conversionContext == null ? new EmptyStorage() : new ConversionContextStorage( conversionContext );
        }
        return contextParams;
    }

    private void setConversionContext( ConversionContext conversionContext ) {
        this.conversionContext = conversionContext;
        CONVERSION_CONTEXT_THREAD_LOCAL.set( conversionContext );
    }

    public XhtmlContent getXhtmlContent() {
        return xhtmlContent;
    }

    @Override
    public void close() {
        super.close();

        if ( CONVERSION_CONTEXT_THREAD_LOCAL.get() == conversionContext )
            CONVERSION_CONTEXT_THREAD_LOCAL.remove();

        conversionContext = null;
    }

    public ParameterAssistant getParameterAssistant() {
        return parameterAssistant;
    }

    public MacroAssistant getMacroAssistant() {
        return macroAssistant;
    }
}