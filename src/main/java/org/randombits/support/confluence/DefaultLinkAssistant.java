/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.support.confluence;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.links.DefaultHrefEvaluator;
import com.atlassian.confluence.content.render.xhtml.links.HrefEvaluator;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContextPathHolder;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.links.LinkManager;
import com.atlassian.confluence.links.OutgoingLink;
import com.atlassian.confluence.pages.*;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.PersonalInformation;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.user.User;
import org.apache.commons.lang.StringUtils;
import org.randombits.support.confluence.LinkAssistant;
import org.randombits.support.core.env.EnvironmentAssistant;

import java.util.EnumSet;

/**
 * Default implementation of the {@link LinkAssistant}.
 *
 * @author David Peterson
 */
public final class DefaultLinkAssistant implements LinkAssistant {

    private enum LinkPart {
        ANCHOR,
        SPACE_KEY,
        ATTACHMENT_NAME,
        PAGE_TITLE,
    }

    private final static char SPACE_SEPARATOR = ':';

    private final static char ATTACHMENT_SEPARATOR = '^';

    private final static char ANCHOR_SEPARATOR = '#';

    private final static char ID_PREFIX = '$';

    private final PermissionManager permissionManager;

    private final ContextPathHolder contextPathHolder;

    private final HrefEvaluator hrefEvaluator;

    private final EnvironmentAssistant environmentAssistant;

    private final LinkManager linkManager;

    private final PageManager pageManager;

    private final AttachmentManager attachmentManager;

    public DefaultLinkAssistant( PermissionManager permissionManager, ContextPathHolder contextPathHolder,
                                 EnvironmentAssistant environmentAssistant,
                                 LinkManager linkManager, PageManager pageManager, AttachmentManager attachmentManager ) {
        this.permissionManager = permissionManager;
        this.contextPathHolder = contextPathHolder;
        this.linkManager = linkManager;
        this.environmentAssistant = environmentAssistant;
        this.pageManager = pageManager;
        this.attachmentManager = attachmentManager;
        hrefEvaluator = new DefaultHrefEvaluator( contextPathHolder );
    }

    /**
     * Checks if the path is absolute from the root of the web application, if so,
     * appends the needed context path, this is not required when it is a relative
     * link or absolute to an external source
     *
     * @param url The URL of the link, eg
     *            /display/DS
     *            ../myaction.action
     *            http://www.customware.net
     * @return This method returns true only when it is absolute from the root of the
     *         web application as this is the only time that the context path is required.
     */
    private boolean requiresContextPath( String url ) {
        return !StringUtils.isEmpty( url ) && url.startsWith( "/" );
    }

    public String getURLForWikiLink( ConversionContext context, String linkText ) {
        ConfluenceEntityObject ceo = getEntityForWikiLink( context, linkText );
        if ( ceo != null ) {
            return getURLForEntity( context, ceo );
        } else {
            if ( requiresContextPath( linkText ) ) {
                linkText = contextPathHolder.getContextPath() + linkText;
            }
            return linkText;
        }
    }

    public String getContextPath() {
        return contextPathHolder.getContextPath();
    }

    @Override
    public boolean saveOutgoingLink( ContentEntityObject sourceContent, ConfluenceEntityObject targetContent ) {
        OutgoingLink link = getOutgoingLink( sourceContent, targetContent );
        if ( link != null ) {
            linkManager.saveLink( link );
            return true;
        }
        return false;
    }

    @Override
    public boolean saveOutgoingLink( ContentEntityObject sourceContent, String spaceKey, String targetPageTitle ) {
        if ( spaceKey == null ) {
            if ( sourceContent instanceof SpaceContentEntityObject ) {
                spaceKey = ( (SpaceContentEntityObject) sourceContent ).getSpaceKey();
            } else {
                return false;
            }
        }

        OutgoingLink link = new OutgoingLink( sourceContent, spaceKey, targetPageTitle );
        linkManager.saveLink( link );
        return true;
    }

    private OutgoingLink getOutgoingLink( ContentEntityObject sourceContent, ConfluenceEntityObject targetContent ) {
        if ( targetContent instanceof Attachment ) {
            targetContent = ( (Attachment) targetContent ).getContent();
        }

        if ( targetContent instanceof SpaceContentEntityObject ) {
            String spaceKey, contentTitle;
            SpaceContentEntityObject sceo = (SpaceContentEntityObject) targetContent;
            spaceKey = sceo.getSpaceKey();
            contentTitle = sceo.getTitle();
            return new OutgoingLink( sourceContent, spaceKey, contentTitle );
        }
        return null;
    }

    @Override
    public boolean saveOutgoingLink( ContentEntityObject sourceContent, String url ) {
        int split = url.indexOf( ':' );
        if ( split >= 0 )
            return saveOutgoingLink( sourceContent, url.substring( 0, split ), url.substring( split + 1 ) );
        else
            return saveOutgoingLink( sourceContent, null, url );
    }

    public String getURLForEntity( ConversionContext context, ConfluenceEntityObject ceo ) {
        return getURLForEntity( context, ceo, null );
    }

    public String getURLForEntity( ConversionContext context, ConfluenceEntityObject ceo, String anchor ) {
        if ( ceo != null ) {
            return hrefEvaluator.createHref( context, ceo, anchor );
        }
        return anchor;
    }

    /**
     * Parses a content name (e.g. 'SPACE:Page' or 'Page^attachment.ext') and
     * returns the entity it identifies.
     *
     * @param context  The context to get spaces/content id info from if not
     *                 specified in the contentName.
     * @param linkText The name of the entity being tracked
     * @return The entity matching the name, or <code>null</code> if it could
     *         not be found.
     */
    public ConfluenceEntityObject getEntityForWikiLink( ConversionContext context, String linkText ) {
        ConfluenceEntityObject content = findEntityForWikiLink( context.getSpaceKey(), linkText, EnumSet.noneOf( LinkPart.class ) );

        if ( content != null ) {
            // Check that the current user is allowed to view the target entity
            User user = environmentAssistant.getValue( User.class );
            return permissionManager.hasPermission( user, Permission.VIEW, content ) ? content : null;
        }

        return null;
    }

    private ConfluenceEntityObject findEntityForWikiLink( String spaceKey, String linkText, EnumSet<LinkPart> processed ) {
        ConfluenceEntityObject content = null;

        // Then, we try the title as is
        if ( AbstractPage.isValidPageTitle( linkText ) ) {
            content = pageManager.getPage( spaceKey, linkText );
        }

        // Then we check if it's an absolute ID
        if ( content == null && linkText.startsWith( String.valueOf( ID_PREFIX ) ) ) {
            try {
                long id = Long.parseLong( linkText.substring( 1 ) );
                content = pageManager.getById( id );
            } catch ( NumberFormatException e ) {
                // Do nothing.
            }
        }

        // Next, we try checking for a '#'
        if ( content == null ) {
            if ( !processed.contains( LinkPart.ANCHOR ) ) {
                int lastAnchor = linkText.lastIndexOf( ANCHOR_SEPARATOR );
                if ( lastAnchor >= 0 ) {
                    content = findEntityForWikiLink( spaceKey, linkText.substring( 0, lastAnchor ), addPart( processed, LinkPart.ANCHOR ) );
                }
            }

            if ( content == null ) {
                if ( !processed.contains( LinkPart.ATTACHMENT_NAME ) ) {
                    int lastAttachment = linkText.lastIndexOf( ATTACHMENT_SEPARATOR );
                    if ( lastAttachment >= 0 ) {
                        String pageTitle = linkText.substring( 0, lastAttachment );
                        String attachmentName = linkText.substring( lastAttachment + 1 );
                        ConfluenceEntityObject page = findEntityForWikiLink( spaceKey, pageTitle, addPart( processed, LinkPart.ATTACHMENT_NAME ) );
                        if ( page instanceof ContentEntityObject ) {
                            content = attachmentManager.getAttachment( (ContentEntityObject) page, attachmentName );
                        }
                    }
                }

                if ( content == null ) {
                    if ( !processed.contains( LinkPart.SPACE_KEY ) ) {
                        int firstSpace = linkText.indexOf( SPACE_SEPARATOR );
                        if ( firstSpace >= 0 ) {
                            String space = linkText.substring( 0, firstSpace );
                            String pageTitle = linkText.substring( firstSpace + 1 );
                            content = findEntityForWikiLink( space, pageTitle, addPart( processed, LinkPart.SPACE_KEY ) );
                        }
                    }
                }
            }
        }
        return content;
    }

    private EnumSet<LinkPart> addPart( EnumSet<LinkPart> processedParts, LinkPart anchor ) {
        EnumSet<LinkPart> clone = processedParts.clone();
        clone.add( anchor );
        return clone;
    }

    /**
     * Generates a wiki link, without any surrounding '[]'s. Eg 'My
     * Page^attachment.ext'.
     *
     * @param context The context being linked from.
     * @param entity  The entity to link to.
     * @return The wiki link text.
     */
    public String getWikiLinkForEntity( ConversionContext context, ConfluenceEntityObject entity ) {
        PageContext ctx = context.getPageContext();
        if ( ctx == null )
            return null;

        StringBuilder buff;
        if ( entity instanceof Attachment )
            buff = getAttachmentWikiLink( (Attachment) entity, ctx );
        else if ( entity instanceof ContentEntityObject )
            buff = getContentEntityWikiLink( (ContentEntityObject) entity, ctx );
        else
            buff = new StringBuilder( "$" ).append( entity.getId() );

        return buff.toString();
    }

    private StringBuilder getAttachmentWikiLink( Attachment attachment, PageContext ctx ) {
        StringBuilder buff;

        if ( !( attachment.getContent() instanceof AbstractPage ) ) {
            buff = new StringBuilder().append( GeneralUtil.getGlobalSettings().getBaseUrl() ).append( attachment.getUrlPath() );
        } else {
            if ( ctx == null || !ctx.getEntity().equals( attachment.getContent() ) )
                buff = getContentEntityWikiLink( attachment.getContent(), ctx );
            else
                buff = new StringBuilder();

            buff.append( "^" ).append( attachment.getFileName() );
        }

        return buff;
    }

    private StringBuilder getContentEntityWikiLink( ContentEntityObject content, PageContext ctx ) {
        StringBuilder buff = new StringBuilder();

        if ( content instanceof SpaceContentEntityObject ) {
            SpaceContentEntityObject spaceContent = (SpaceContentEntityObject) content;
            if ( !StringUtils.equals( spaceContent.getSpaceKey(), ctx.getSpaceKey() ) )
                buff.append( spaceContent.getSpaceKey() ).append( ":" );

            if ( content instanceof Page ) {
                Page page = (Page) content;
                buff.append( page.getTitle() );
            } else if ( content instanceof BlogPost ) {
                BlogPost blogPost = (BlogPost) content;
                buff.append( blogPost.getDatePath() ).append( "/" ).append( blogPost.getTitle() );
            } else {
                buff.append( "$" ).append( content.getIdAsString() );
            }
        } else if ( content instanceof PersonalInformation ) {
            buff.append( "~" ).append( ( (PersonalInformation) content ).getUsername() );
        } else {
            buff.append( "$" ).append( content.getIdAsString() );
        }
        return buff;
    }
}
