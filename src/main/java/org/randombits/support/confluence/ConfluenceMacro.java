/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.support.confluence;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.support.core.param.ParameterAssistant;

import java.util.Map;

/**
 * @author David Peterson
 */
public abstract class ConfluenceMacro implements Macro {

    private final MacroAssistant macroAssistant;

    public ConfluenceMacro( MacroAssistant macroAssistant ) {
        this.macroAssistant = macroAssistant;
    }

    /**
     * @param environmentAssistant The EnvironmentAssistant.
     * @param xhtmlContent         The XhtmlContent.
     * @deprecated Use {@link #ConfluenceMacro(MacroAssistant)} instead.
     */
    @SuppressWarnings("UnusedParameters")
    @Deprecated
    public ConfluenceMacro( EnvironmentAssistant environmentAssistant, XhtmlContent xhtmlContent ) {
        this.macroAssistant = new DefaultMacroAssistant( environmentAssistant, xhtmlContent, null );
    }

    public String execute( Map<String, String> params, String body, ConversionContext conversionContext ) throws MacroExecutionException {
        MacroInfo info = createMacroInfo( params, body, conversionContext );
        try {
            return execute( info );
        } finally {
            closeMacroInfo( info );
        }
    }

    protected EnvironmentAssistant getEnvironmentAssistant() {
        return macroAssistant.getEnvironmentAssistant();
    }

    /**
     * This method is called to create a new MacroInfo instance. Override it to
     * return other subclasses if so desired.
     *
     * @param params            The macro parameter map.
     * @param body              The body of the macro.
     * @param conversionContext The conversion context.
     * @return The new MacroInfo instance.
     * @throws com.atlassian.confluence.macro.MacroExecutionException
     *          if there is a problem while process.ing.
     */
    protected MacroInfo createMacroInfo( Map<String, String> params, String body, ConversionContext conversionContext ) throws MacroExecutionException {
        return new MacroInfo( params, body, conversionContext, macroAssistant );
    }

    /**
     * This method is called to close the MacroInstance created by {@link #createMacroInfo(java.util.Map, String, com.atlassian.confluence.content.render.xhtml.ConversionContext)}
     * By default it simply calls {@link org.randombits.support.confluence.MacroInfo#close()}
     *
     * @param info The context to close.
     */
    protected void closeMacroInfo( MacroInfo info ) {
        info.close();
    }

    /**
     * Returns XHTML or plain text content (depending on the value returned from
     * {@link #getBodyType()}
     *
     * @param info The macro context.
     * @return The XHTML or wiki content.
     * @throws com.atlassian.confluence.macro.MacroExecutionException
     *          if there is a problem while executing.
     */
    protected abstract String execute( MacroInfo info ) throws MacroExecutionException;

    public XhtmlContent getXhtmlContent() {
        return macroAssistant.getXhtmlContent();
    }

    public ParameterAssistant getParameterAssistant() {
        return macroAssistant.getParameterAssistant();
    }

    public MacroAssistant getMacroAssistant() {
        return macroAssistant;
    }
}
