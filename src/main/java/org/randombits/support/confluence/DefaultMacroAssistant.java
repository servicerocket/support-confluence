package org.randombits.support.confluence;

import com.atlassian.confluence.xhtml.api.XhtmlContent;
import org.randombits.support.confluence.MacroAssistant;
import org.randombits.support.core.env.EnvironmentAssistant;
import org.randombits.support.core.param.ParameterAssistant;

/**
 * Default implementation of {@link MacroAssistant}.
 */
public class DefaultMacroAssistant implements MacroAssistant {

    private final EnvironmentAssistant environmentAssistant;

    private final XhtmlContent xhtmlContent;

    private final ParameterAssistant parameterAssistant;

    public DefaultMacroAssistant( EnvironmentAssistant environmentAssistant, XhtmlContent xhtmlContent, ParameterAssistant parameterAssistant ) {
        this.environmentAssistant = environmentAssistant;
        this.xhtmlContent = xhtmlContent;
        this.parameterAssistant = parameterAssistant;
    }

    @Override
    public EnvironmentAssistant getEnvironmentAssistant() {
        return environmentAssistant;
    }

    @Override
    public ParameterAssistant getParameterAssistant() {
        return parameterAssistant;
    }

    @Override
    public XhtmlContent getXhtmlContent() {
        return xhtmlContent;
    }
}
