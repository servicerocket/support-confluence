package org.randombits.support.confluence.env;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.User;
import org.randombits.support.core.env.SimpleEnvironmentProvider;

/**
 * Returns the current user when in Confluence.
 */
public class AuthenticatedUserProvider extends SimpleEnvironmentProvider<User> {

    @Override
    public User getValue() {
        return AuthenticatedUserThreadLocal.getUser();
    }
}
