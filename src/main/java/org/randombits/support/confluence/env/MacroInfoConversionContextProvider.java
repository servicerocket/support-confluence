package org.randombits.support.confluence.env;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import org.randombits.support.confluence.MacroInfo;
import org.randombits.support.core.env.SimpleEnvironmentProvider;

/**
 * Finds current {@link ConversionContext} to render with, via {@link org.randombits.support.confluence.MacroInfo#getCurrentConversionContext()}.
 */
public class MacroInfoConversionContextProvider extends SimpleEnvironmentProvider<ConversionContext> {

    @Override
    public ConversionContext getValue() {
        return MacroInfo.getCurrentConversionContext();
    }
}
