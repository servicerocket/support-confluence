package org.randombits.support.confluence.env;

import com.atlassian.confluence.core.ContentEntityObject;
import org.randombits.support.confluence.PageInfo;
import org.randombits.support.core.env.AbstractEnvironmentProvider;

/**
 * Provides access to the {@link ContentEntityObject} being executed within the context of a
 * {@link PageInfo} instance, usually via a {@link org.randombits.support.confluence.ConfluenceMacro}
 * implementation.
 */
public class PageInfoConfluenceContentProvider extends AbstractEnvironmentProvider {

    public PageInfoConfluenceContentProvider() {
        super( ContentEntityObject.class );
    }

    @Override
    public <T> T getValue( Class<T> type ) {
        if ( ContentEntityObject.class.isAssignableFrom( type ) ) {
            ContentEntityObject content = PageInfo.getCurrentContent();
            if ( type.isInstance( content ) ) {
                return type.cast( content );
            }
        }
        return null;
    }
}
