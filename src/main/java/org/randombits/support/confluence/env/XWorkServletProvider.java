package org.randombits.support.confluence.env;

import com.opensymphony.webwork.ServletActionContext;
import org.randombits.support.core.env.servlet.AbstractServletProvider;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides access to HttpServletResponse/etc when in the context of an XWork Action.
 */
public class XWorkServletProvider extends AbstractServletProvider {

    /**
     * Returns a weight of 100.
     *
     * @return
     */
    @Override
    public int getWeight() {
        return 100;
    }

    @Override
    protected ServletConfig getServletConfig() {
        return ServletActionContext.getServletConfig();
    }

    @Override
    protected HttpServletResponse getHttpServletResponse() {
        return ServletActionContext.getResponse();
    }

    @Override
    protected HttpServletRequest getHttpServletRequest() {
        return ServletActionContext.getRequest();
    }
}
