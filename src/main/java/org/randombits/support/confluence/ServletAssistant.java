package org.randombits.support.confluence;

import com.atlassian.user.User;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public interface ServletAssistant {

    User getCurrentUser();

    HttpServletRequest getRequest();

    /**
     * Returns the request as a {@link com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper}, if the current
     * request supports it. Otherwise, <code>null</code> is returned.
     *
     * @return The request as a multipart handler, or <code>null</code>.
     */
    MultiPartRequestWrapper getMultiPartRequest();

    /**
     * Returns the current HttpServletResponse, if it's available.
     *
     * @return The response, or <code>null</code>.
     */
    HttpServletResponse getResponse();

    ServletConfig getServletConfig();

    ServletContext getServletContext();

    HttpSession getSession();

    /**
     * Returns the full path, including the web application context path,
     * if the HttpServlet request is available.
     *
     * @param webappRelativePath The path from the root of the web application.
     * @return The full path.
     */
    String getFullPath( String webappRelativePath );
}
