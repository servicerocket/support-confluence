/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.support.confluence;

import com.atlassian.confluence.core.ConfluenceActionSupport;

import java.util.List;
import java.util.ResourceBundle;

/**
 * This is a support implementation of ConfluenceActionSupport to assist with
 * getting around a few issues with uploaded plugin. In particular, this class
 * will let you use the getText methods to load properties from local properties
 * files.
 * 
 * @author David Peterson
 */
public class PluginActionSupport extends ConfluenceActionSupport {

    @Override public String getText( String key ) {
        ClassLoader oldLoader = overrideThreadClassloader();
        String text = super.getText( key );
        resetThreadClassLoader( oldLoader );
        if ( text == null || text.equals( key ) )
            text = super.getText( key );
        return text;
    }

    @Override public String getText( String key, Object[] params ) {
        ClassLoader oldLoader = overrideThreadClassloader();
        String text = super.getText( key, params );
        resetThreadClassLoader( oldLoader );
        if ( text == null || text.equals( key ) )
            text = super.getText( key, params );
        return text;
    }

    // due to overriding an existing untyped method.
    @Override @SuppressWarnings("unchecked")
    public String getText( String key, List params ) {
        ClassLoader oldLoader = overrideThreadClassloader();
        String text = super.getText( key, params );
        resetThreadClassLoader( oldLoader );
        if ( text == null || text.equals( key ) )
            text = super.getText( key, params );
        return text;
    }

    @Override public String getTextStrict( String key ) {
        ClassLoader oldLoader = overrideThreadClassloader();
        String text = super.getTextStrict( key );
        resetThreadClassLoader( oldLoader );
        if ( text == null )
            text = super.getTextStrict( key );
        return text;
    }

    @Override public String getText( String key, String locale ) {
        ClassLoader oldLoader = overrideThreadClassloader();
        String text = super.getText( key, locale );
        resetThreadClassLoader( oldLoader );
        if ( text == null || text.equals( key ) )
            text = super.getText( key, locale );
        return text;
    }

    // due to overriding an existing untyped method.
    @Override @SuppressWarnings("unchecked")
    public String getText( String key, String locale, List params ) {
        ClassLoader oldLoader = overrideThreadClassloader();
        String text = super.getText( key, locale, params );
        resetThreadClassLoader( oldLoader );
        if ( text == null || text.equals( key ) )
            text = super.getText( key, locale, params );
        return text;
    }

    @Override public ResourceBundle getTexts() {
        ClassLoader oldLoader = overrideThreadClassloader();
        ResourceBundle texts = super.getTexts();
        resetThreadClassLoader( oldLoader );
        return texts;
    }

    @Override public ResourceBundle getTexts( String string ) {
        ClassLoader oldLoader = overrideThreadClassloader();
        ResourceBundle texts = super.getTexts( string );
        resetThreadClassLoader( oldLoader );
        return texts;
    }

    private void resetThreadClassLoader( ClassLoader previousClassLoader ) {
        if ( previousClassLoader != null ) {
            Thread.currentThread().setContextClassLoader( previousClassLoader );
        }
    }

    private ClassLoader overrideThreadClassloader() {
        ClassLoader previousClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader( getClass().getClassLoader() );
        return previousClassLoader;
    }
}
