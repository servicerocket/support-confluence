package org.randombits.support.confluence.convert.search;

import com.atlassian.confluence.spaces.Space;

/**
 * Converts a {@link com.atlassian.confluence.search.v2.SearchResult} into a {@link Space}.
 */
public class SearchResultToSpaceConverter extends AbstractSearchResultConverter<Space> {

    public SearchResultToSpaceConverter() {
        super( Space.CONTENT_TYPE );
    }
}
