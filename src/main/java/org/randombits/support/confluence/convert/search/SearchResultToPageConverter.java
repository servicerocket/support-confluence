package org.randombits.support.confluence.convert.search;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.search.v2.SearchResult;

/**
 * Converts a {@link SearchResult} of type 'page' into a Confluence {@link Page} instance,
 * if a matching page exists. No permission checking is done.
 */
public class SearchResultToPageConverter extends AbstractSearchResultConverter<Page> {

    public SearchResultToPageConverter() {
        super( Page.CONTENT_TYPE );
    }
}
