package org.randombits.support.confluence.convert.user;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.user.User;
import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

/**
 * Converts an Atlassian {@link User} to a SAL {@link UserProfile}.
 */
public class UserToUserProfileConverter extends AbstractConverter<User, UserProfile> {

    private final UserManager userManager;

    public UserToUserProfileConverter( UserManager userManager ) {
        super( ConversionCost.DATABASE_LOOKUP );
        this.userManager = userManager;
    }

    @Override
    protected UserProfile convert( User user ) throws ConversionException {
        return userManager.getUserProfile( user.getName() );
    }
}
