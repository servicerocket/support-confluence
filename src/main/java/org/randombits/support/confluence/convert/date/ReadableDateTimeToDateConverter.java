package org.randombits.support.confluence.convert.date;

import org.joda.time.ReadableDateTime;
import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

import java.util.Date;

/**
 * Converts a {@link ReadableDateTime} into a Java {@link Date} object.
 */
public class ReadableDateTimeToDateConverter extends AbstractConverter<ReadableDateTime, Date> {

    public ReadableDateTimeToDateConverter() {
        super( ConversionCost.CREATE_SIMPLE_OBJECT );
    }

    @Override
    protected Date convert( ReadableDateTime readableDateTime ) throws ConversionException {
        return readableDateTime.toDateTime().toDate();
    }
}
