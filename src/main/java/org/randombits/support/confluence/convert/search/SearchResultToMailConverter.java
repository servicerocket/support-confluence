package org.randombits.support.confluence.convert.search;

import com.atlassian.confluence.mail.Mail;

/**
 * Converts a {@link com.atlassian.confluence.search.v2.SearchResult} to {@link Mail}.
 */
public class SearchResultToMailConverter extends AbstractSearchResultConverter<Mail> {

    public SearchResultToMailConverter() {
        super( Mail.CONTENT_TYPE );
    }
}
