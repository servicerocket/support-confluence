package org.randombits.support.confluence.convert.search;

import com.atlassian.confluence.pages.BlogPost;

/**
 * Converts a {@link com.atlassian.confluence.search.v2.SearchResult} of type 'page' into a Confluence {@link com.atlassian.confluence.pages.Page} instance,
 * if a matching page exists. No permission checking is done.
 */
public class SearchResultToBlogPostConverter extends AbstractSearchResultConverter<BlogPost> {

    public SearchResultToBlogPostConverter() {
        super( BlogPost.CONTENT_TYPE );
    }
}
