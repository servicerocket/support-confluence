package org.randombits.support.confluence.convert.search;

import com.atlassian.confluence.pages.Attachment;

/**
 * Converts a {@link com.atlassian.confluence.search.v2.SearchResult} to an {@link Attachment}.
 */
public class SearchResultToAttachmentConverter extends AbstractSearchResultConverter<Attachment> {

    public SearchResultToAttachmentConverter() {
        super( Attachment.CONTENT_TYPE );
    }
}
