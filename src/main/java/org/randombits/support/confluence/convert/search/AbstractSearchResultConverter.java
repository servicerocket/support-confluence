package org.randombits.support.confluence.convert.search;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.search.v2.SearchResult;
import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionException;
import org.springframework.beans.factory.annotation.Autowired;

import static org.randombits.support.core.convert.ConversionCost.*;

/**
 * Abstract base class for converting {@link SearchResult} values to {@link ConfluenceEntityObject} instances.
 */
public abstract class AbstractSearchResultConverter<T extends ConfluenceEntityObject> extends AbstractConverter<SearchResult, T> {

    private AnyTypeDao anyTypeDao;

    private final String type;

    public AbstractSearchResultConverter( String type ) {
        super( DATABASE_LOOKUP.and( CREATE_COMPLEX_OBJECT ) );
        this.type = type;
    }

    protected boolean supportsSearchResult( SearchResult result ) {
        return this.type.equals( result.getType() );
    }

    @Override
    public boolean canConvert( Object source, Class<?> targetType ) {
        if ( source instanceof SearchResult ) {
            SearchResult result = (SearchResult) source;
            // Check we support this particular search result type.
            if ( supportsSearchResult( result ) )
                return super.canConvert( source, targetType );
        }
        return false;
    }

    @Override
    protected T convert( SearchResult searchResult ) throws ConversionException {
        if ( supportsSearchResult( searchResult ) ) {
            Object value = anyTypeDao.findByHandle( searchResult.getHandle() );
            if ( getTargetType().isInstance( value ) ) {
                return getTargetType().cast( value );
            }
        }
        return null;
    }

    protected AnyTypeDao getAnyTypeDao() {
        return anyTypeDao;
    }

    @Autowired
    public void setAnyTypeDao( AnyTypeDao anyTypeDao ) {
        this.anyTypeDao = anyTypeDao;
    }
}
