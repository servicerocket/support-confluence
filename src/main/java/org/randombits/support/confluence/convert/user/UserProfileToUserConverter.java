package org.randombits.support.confluence.convert.user;

import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.user.User;
import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

/**
 * Converts from a SAL {@link UserProfile} into an Atlassian {@link User}.
 */
public class UserProfileToUserConverter extends AbstractConverter<UserProfile, User> {

    private final UserAccessor userAccessor;

    public UserProfileToUserConverter( UserAccessor userAccessor ) {
        super( ConversionCost.DATABASE_LOOKUP );
        this.userAccessor = userAccessor;
    }

    @Override
    protected User convert( UserProfile userProfile ) throws ConversionException {
        return userAccessor.getUser( userProfile.getUsername() );
    }
}
