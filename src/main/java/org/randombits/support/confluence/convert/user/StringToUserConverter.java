package org.randombits.support.confluence.convert.user;

import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.user.User;
import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

/**
 * Converts a username string into a {@link User} instance, if it is available.
 */
public class StringToUserConverter extends AbstractConverter<String, User> {

    private final UserAccessor userAccessor;

    public StringToUserConverter( UserAccessor userAccessor ) {
        super( ConversionCost.DATABASE_LOOKUP );
        this.userAccessor = userAccessor;
    }

    @Override
    protected User convert( String username ) throws ConversionException {
        return userAccessor.getUser( username );
    }
}
