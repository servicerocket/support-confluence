package org.randombits.support.confluence.convert.date;

import org.joda.time.DateTime;
import org.randombits.support.core.convert.AbstractConverter;
import org.randombits.support.core.convert.ConversionCost;
import org.randombits.support.core.convert.ConversionException;

import java.util.Date;

/**
 * Converts a standard {@link Date} to a Joda {@link DateTime}.
 */
public class DateToDateTimeConverter extends AbstractConverter<Date, DateTime> {

    public DateToDateTimeConverter() {
        super( ConversionCost.CREATE_SIMPLE_OBJECT );
    }

    @Override
    protected DateTime convert( Date date ) throws ConversionException {
        return new DateTime( date );
    }
}
