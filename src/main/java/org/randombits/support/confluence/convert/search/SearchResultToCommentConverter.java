package org.randombits.support.confluence.convert.search;

import com.atlassian.confluence.pages.Comment;

/**
 * Converts a {@link com.atlassian.confluence.search.v2.SearchResult} into a {@link Comment},
 * if it is the correct type of result.
 */
public class SearchResultToCommentConverter extends AbstractSearchResultConverter<Comment> {

    public SearchResultToCommentConverter() {
        super( Comment.CONTENT_TYPE );
    }
}
