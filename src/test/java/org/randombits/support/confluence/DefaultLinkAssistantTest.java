package org.randombits.support.confluence;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContextPathHolder;
import com.atlassian.confluence.links.LinkManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.randombits.support.confluence.DefaultLinkAssistant;
import org.randombits.support.core.env.EnvironmentAssistant;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Tests {@link DefaultLinkAssistant}
 */
public class DefaultLinkAssistantTest {

    private DefaultLinkAssistant linkAssistant;

    private PermissionManager permissionManager;

    private ContextPathHolder contextPathHolder;

    private EnvironmentAssistant environmentAssistant;

    private LinkManager linkManager;

    private PageManager pageManager;

    private AttachmentManager attachmentManager;

    private ConversionContext conversionContext;

    @Before
    public void setUp() {
        permissionManager = mock( PermissionManager.class );
        contextPathHolder = mock( ContextPathHolder.class );
        environmentAssistant = mock( EnvironmentAssistant.class );
        linkManager = mock( LinkManager.class );
        pageManager = mock( PageManager.class );
        attachmentManager = mock( AttachmentManager.class );

        conversionContext = mock( ConversionContext.class );

        linkAssistant = new DefaultLinkAssistant( permissionManager, contextPathHolder, environmentAssistant,
                linkManager, pageManager, attachmentManager );
    }

    @After
    public void tearDown() {
        linkAssistant = null;
        permissionManager = null;
        contextPathHolder = null;
        environmentAssistant = null;
        linkManager = null;
        pageManager = null;
        attachmentManager = null;
    }

    @Test
    public void testGetEntityForSimpleWikiLink() {

        String spaceKey = "KEY";
        String pageName = "Name of Page";

        Page page = mock( Page.class );

        when( conversionContext.getSpaceKey() ).thenReturn( spaceKey );
        when( pageManager.getPage( spaceKey, pageName ) ).thenReturn( page );
        when( permissionManager.hasPermission( null, Permission.VIEW, page ) ).thenReturn( true );

        ConfluenceEntityObject entity = linkAssistant.getEntityForWikiLink( conversionContext, pageName );
        assertSame( page, entity );

        verify( conversionContext, times( 1 ) ).getSpaceKey();
        verify( pageManager, times( 1 ) ).getPage( spaceKey, pageName );
    }

    @Test
    public void testGetEntityForComplexWikiLink() {
        String spaceKey = "KEY";
        String pageName = "Page:Name !@#$%^&*()\"<>?";
        String attachmentName = "attachment.ext";

        Page page = mock( Page.class );
        Attachment attachment = mock( Attachment.class );

        // Basic checks
        when( conversionContext.getSpaceKey() ).thenReturn( spaceKey );
        when( permissionManager.hasPermission( null, Permission.VIEW, page ) ).thenReturn( true );
        when( permissionManager.hasPermission( null, Permission.VIEW, attachment ) ).thenReturn( true );

        when( pageManager.getPage( spaceKey, pageName ) ).thenReturn( page );
        when( attachmentManager.getAttachment( page, attachmentName ) ).thenReturn( attachment );

        when( pageManager.getPage( "FOO", pageName ) ).thenReturn( page );

        Object value;

        // Test 1: Full page name matches a page title, including the 'key' and attachment name.
        value = linkAssistant.getEntityForWikiLink( conversionContext, pageName );
        assertSame( page, value );

        // Test 2: Full Page name + attachment
        value = linkAssistant.getEntityForWikiLink( conversionContext, pageName + "^" + attachmentName );
        assertSame( attachment, value );

        // Test 3: Other Space has the page.
        value = linkAssistant.getEntityForWikiLink( conversionContext, "FOO:" + pageName );
        assertSame( page, value );

        // Test 4: Other space has the attachment
        value = linkAssistant.getEntityForWikiLink( conversionContext, "FOO:" + pageName + "^" + attachmentName );
        assertSame( attachment, value );

        verify( pageManager, times( 2 ) ).getPage( spaceKey, pageName );
        verify( pageManager, times( 2 ) ).getPage( "FOO", pageName );
        verify( attachmentManager, times( 2 ) ).getAttachment( page, attachmentName );
    }
}
